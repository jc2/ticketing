if (Meteor.isClient) {
  Template.registerHelper('and',function(a, b){
    return a && b;
  });
  Template.registerHelper('or',function(a, b){
    return a || b;
  });
  Template.registerHelper('or_1b',function(a, b){
    return a || !b;
  });
  Template.registerHelper('and_1b',function(a, b){
    return a && !b;
  });
  Template.registerHelper('and3_1b_1c',function(a, b, c){
    return a && !b && !c;
  });
  Template.registerHelper('not',function(a){
    return !a;
  });
  Template.registerHelper('equal',function(a, b){
    return a == b;
  });
}
