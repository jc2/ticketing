if (Meteor.isClient) {
  Template.listForm.events({
    "submit #formListTickets": function(event, template){
      Session.set("ticketsList", null);
      var name = event.target.name.value;
      var country = event.target.country.value;
      var state = event.target.state.value;
      var attack_type = event.target.attack_type.value;
      var show = event.target.show.value;

      var params ={
        'from': '0',
        'to': show,
        'filter': 'state eq ' + state,
        'orderby': ' '
      }

      Meteor.call("getTickets", params, function(error, result){
        if(error){
          console.log("error", error);
        }
        if(result){
           Session.set("ticketsList", JSON.parse(result.content));
        }
      });
      //console.log(Session.get("ticketsList"));
      $('#collapseFilter').collapse('hide');
      return false;
    },
  });
  Template.tickets.helpers({
    ticketsList: function(){
      if (Session.get("firstLoad")) {
        //console.log( Session.get("ticketsList"));
        return Session.get("ticketsList");
      }
      else{
        Session.set("firstLoad", true);
        var params ={
          'from': '0',
          'to': '10',
          'filter': 'state eq 4' ,
          'orderby': ' '
        }

        Meteor.call("getTickets", params, function(error, result){
          if(error){
            console.log("error", error);
          }
          if(result){
             console.log(JSON.parse(result.content));
             Session.set("ticketsList", JSON.parse(result.content));
          }
        });
      }

    },

  });
  Template.tickets.events({
    "click .ticketDetails": function(event, template){
      event.preventDefault();
      var id = event.target.getAttribute("data-id");
      var providers_url = event.target.getAttribute("data-providers-url");
      var whois_url = event.target.getAttribute("data-whois-url");
      //Session.set("ticketProviders-"+id, null);
      if (Session.get("ticketProviders-"+id)){
        return null;
      }
      if (providers_url && whois_url) {
        Meteor.call("getProvidersAndWhoIs", {'providers_url': providers_url, 'whois_url': whois_url}, function(error, result){
          if(error){
            console.log("error", error);
          }
          if(result){
            console.log(JSON.parse(result.content));
            return Session.set("ticketProviders-"+id, JSON.parse(result.content));
          }
        });
      }
    },
    "click .paginatorBtn": function(event, template){
      event.preventDefault();
      Session.set("ticketsList", null);
      var url = event.target.getAttribute("data-url");
      if (url) {
        Meteor.call("getTicket", url, function(error, result){
          if(error){
            console.log("error", error);
          }
          if(result){
            return Session.set("ticketsList", JSON.parse(result.content));
          }
        });
      }
    }
  });
  Template.ticket.helpers({
    providers: function(id){
      var providers = Session.get("ticketProviders-"+id);
      if (providers) {
        providers["ticket_id"] = id;
      }
      console.log(providers);
      return providers;
    }
  });
  Template.ticket.events({
    "submit .contactForm": function(event, template){
      var value = event.target.contact.value;
      var url = event.target.contact.getAttribute("data-url");
      var providers_url = event.target.contact.getAttribute("data-providers-url");
      var ticket_id = event.target.contact.getAttribute("data-ticket-id");
      if (value && value != "") {
        data = {
          "url": url,
          "info": {
            "contact": value,
          }
        }
        Session.set("ticketProviders-"+ticket_id, null);
        Meteor.call("setContact", data, function(error, result){
          if(error){
            console.log("error", error);
          }
          if(result){
            console.log(result);

            if (providers_url) {
              Meteor.call("getProviders", providers_url, function(error, result){
                if(error){
                  console.log("error", error);
                }
                if(result){
                  return Session.set("ticketProviders-"+ticket_id, JSON.parse(result.content));
                }
              });
            }
            return false;
          }
        });
      }
      return false;
    }
  });
}

if (Meteor.isServer) {
  Meteor.startup(function () {
  });
  Meteor.methods({
    getTickets:function(params){
      return Meteor.http.get('http://192.168.243.44:8081/restService/v1/tickets', {'params': params, 'headers':
      {'Authorization': 'eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJteXNlbGYiLCJzdWIiOiJURVNUIiwiZXhwIjoxNDQ2MTQ3MDM5LCJyb2xlcyI6IlJPTEVfQURNSU4ifQ.000KUeNJC-8daF6RWhYLtAeTFc7rGo-W3AiEAEp-rcI'} });
    },
    getProvidersAndWhoIs:function(urls){
      Meteor.http.get(urls.whois_url, {'headers':
      {'Authorization': 'eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJteXNlbGYiLCJzdWIiOiJURVNUIiwiZXhwIjoxNDQ2MTQ3MDM5LCJyb2xlcyI6IlJPTEVfQURNSU4ifQ.000KUeNJC-8daF6RWhYLtAeTFc7rGo-W3AiEAEp-rcI'} });
      return Meteor.http.get(urls.providers_url, {'headers':
      {'Authorization': 'eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJteXNlbGYiLCJzdWIiOiJURVNUIiwiZXhwIjoxNDQ2MTQ3MDM5LCJyb2xlcyI6IlJPTEVfQURNSU4ifQ.000KUeNJC-8daF6RWhYLtAeTFc7rGo-W3AiEAEp-rcI'} });
    },
    getProviders:function(providers_url){
      return Meteor.http.get(providers_url, {'headers':
      {'Authorization': 'eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJteXNlbGYiLCJzdWIiOiJURVNUIiwiZXhwIjoxNDQ2MTQ3MDM5LCJyb2xlcyI6IlJPTEVfQURNSU4ifQ.000KUeNJC-8daF6RWhYLtAeTFc7rGo-W3AiEAEp-rcI'} });
    },
    setContact:function(data){
      console.log(data);
      return Meteor.http.post(data.url ,{'data': data.info, 'headers':
      {'Authorization': 'eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJteXNlbGYiLCJzdWIiOiJURVNUIiwiZXhwIjoxNDQ2MTQ3MDM5LCJyb2xlcyI6IlJPTEVfQURNSU4ifQ.000KUeNJC-8daF6RWhYLtAeTFc7rGo-W3AiEAEp-rcI'} });
    }
  });
}
